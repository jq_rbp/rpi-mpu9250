CXX=g++
CXXFLAGS= -Wall -g -O2
CXX_OPTS= -c -DMPU9250 -DMPU_DEBUGOFF -I.. `pkg-config --cflags opencv`
LDFLAGS=-lpthread -lwebsockets -lm -lrt `pkg-config --libs opencv`

SOURCESDIR=src
SOURCES=main.c \
	configUtils.c \
	I2Cdev/I2Cdev.c \
	timeUtils.c \
	sensor.c \
	eMPL/inv_mpu.c \
	eMPL/inv_mpu_dmp_motion_driver.c \
	threadUtils.c \
	webUtils.c \
	ocvCam.c

OBJECTSDIR=obj
OBJECTS=$(addprefix $(OBJECTSDIR)/, ${SOURCES:.c=.o})

INSTALL=install

PROG=rpi-mpu9250

${OBJECTSDIR}/%.o: ${SOURCESDIR}/%.c
	@mkdir -p $(dir $@)
	$(CXX) $(CXXFLAGS) $(CXX_OPTS) $< -o $@

$(PROG): ${OBJECTS}
	$(CXX) $(LDFLAGS) $(CXXFLAGS) $^ -o $(PROG)

all: $(PROG)

clean:
	rm -rf ${OBJECTSDIR}/*
	rm -rf $(PROG)

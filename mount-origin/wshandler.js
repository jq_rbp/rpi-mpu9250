var head = 0, tail = 0, ring = new Array();
var myJSON;
var ws;
var calibmag = false;
// var imageNr = 0; // Serial number of current image
// var finished = new Array(); // References to img objects which have finished downloading
// var paused = false;

function get_appropriate_ws_url(extra_url)
{
	var pcol;
	var u = document.URL;

	/*
	 * We open the websocket encrypted if this page came on an
	 * https:// url itself, otherwise unencrypted
	 */

	if (u.substring(0, 5) === "https") {
		pcol = "wss://";
		u = u.substr(8);
	} else {
		pcol = "ws://";
		if (u.substring(0, 4) === "http")
			u = u.substr(7);
	}

	u = u.split("/");

	/* + "/xxx" bit is for IE10 workaround */

	return pcol + u[0] + "/" + extra_url;
}

function new_ws(urlpath, protocol)
{
	if (typeof MozWebSocket != "undefined")
		return new MozWebSocket(urlpath, protocol);

	return new WebSocket(urlpath, protocol);
}

function chkMyJSON(_val) {
	if(myJSON == null) return false;
	if(myJSON == undefined) return false;
	if(myJSON[_val] == null) return false;
	if(myJSON[_val] == undefined) return false;
	return true;
}

function loadMyJson(_str) {
	myJSON = null;
	try {
		myJSON = JSON.parse(_str);
	} catch (e) {
		return false;
	}

	return true;
}

function setMyJson(_val) {
	if(chkMyJSON(_val)) {
		document.getElementById(_val).value = myJSON[_val];
		return true;
	}
	return false;
}

document.addEventListener("DOMContentLoaded", function() {

	ws = new_ws(get_appropriate_ws_url(""), "lws-minimal");
	try {
		ws.onopen = function() {
			document.getElementById("r").disabled = 0;
		};
	
		ws.onmessage =function got_packet(msg) {
			var n, s = "";
			var msgdata = msg.data;

			if(loadMyJson(msgdata)) {
				if(!document.getElementById("inputdisplay").hidden) {
					document.getElementById("inputdisplay").hidden = true;
					document.getElementById("compass").style.display = 'block';
				}

				//object quaternion
				setMyJson("qw"); setMyJson("qx"); setMyJson("qy"); setMyJson("qz");

				//heading
				if(chkMyJSON("ch")) {
					document.getElementById("ch").value = myJSON["ch"];
					document.getElementById("compass-img").style.transform = 'rotate(' + (360 - myJSON.ch) + 'deg)'; 
					if(!calibmag)document.getElementById("stat").value = "run"
				}

				//opencv process info
				setMyJson("cvt"); 
				if(setMyJson("cvi")) {
					createImageLayer();
				}

				//index message
				setMyJson("qno");

				//magnetometer values
				setMyJson("mvx"); setMyJson("mvy"); setMyJson("mvz"); 

				//magnetometer calibration
				setMyJson("mbx"); setMyJson("mby"); setMyJson("mbz"); 
				setMyJson("msx"); setMyJson("msy"); setMyJson("msz");

				if(chkMyJSON("mcx")) {
					if(chkMyJSON("mci")) {
						addpoints(myJSON.mvx/3, myJSON.mvy/3, myJSON.mvz/3, myJSON.mcx);
						document.getElementById("stat").value = "calibno:" + myJSON.mci + "/" + myJSON.mcx;
						if(myJSON.mci >= myJSON.mcx - 2) calibmag = false;
					}
				}
			}

			ring[head] = msgdata + "\n";
			head = (head + 1) % 50;
			if (tail === head)
				tail = (tail + 1) % 50;
	
			n = tail;
			do {
				s = s + ring[n];
				n = (n + 1) % 50;
			} while (n !== head);
			
			document.getElementById("r").value = s; 
			document.getElementById("r").scrollTop =
			document.getElementById("r").scrollHeight;
		};
	
		ws.onclose = function(){
			document.getElementById("r").disabled = 1;
		};
	} catch(exception) {
		alert("<p>Error " + exception);  
	}

}, false);

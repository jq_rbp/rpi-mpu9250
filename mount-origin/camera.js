var imageNr = 0; // Serial number of current image
var finished = new Array(); // References to img objects which have finished downloading
var paused = false;

window.addEventListener("load", createImageLayer, false);

function createImageLayer() {
  var imgIdx = document.getElementById("cvi").value;
  var img = new Image();
  img.onload = imageOnload;
  img.onclick = imageOnclick;
  if(imgIdx >= 0) {
    img.src = "/assets/images/ocv/ocv_image_" + imgIdx + ".jpg"
  } else {
    img.src = "data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=";
  }
  var webcam = document.getElementById("webcam");
  webcam.insertBefore(img, webcam.firstChild);
}

// Two layers are always present (except at the very beginning), to avoid flicker
function imageOnload() {
  this.style.zIndex = imageNr; // Image finished, bring to front!
  while (1 < finished.length) {
    var del = finished.shift(); // Delete old image(s) from document
    del.parentNode.removeChild(del);
  }
  finished.push(this);
  // if (!paused) {
  //   setTimeout(createImageLayer, document.getElementById("cvt").value);
  // }
}

function imageOnclick() { // Clicking on the image will pause the stream
  paused = !paused;
  if (!paused) createImageLayer();
}
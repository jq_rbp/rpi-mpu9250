# rpi-mpu9250

interfacing mpu9250 on embedded device with linux OS (raspbian strecth) 

Dependencies:

1. libwebsockets >= 3.0.0

    ref: https://github.com/warmcat/libwebsockets

2. libopencv ~ 2.4

    can be installed from raspbian apt install.

3. pkg-config, g++, make, cmake (for libwebsockets compilation).

Compilation:

1. Install all dependencies.

2. run "make".
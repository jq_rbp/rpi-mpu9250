#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <cstdlib>
#include <ctype.h>
#include "configUtils.h"

#define MAXSTRLEN 80
#define PRECNUM 10000
struct config_file_parameters params;

float defMagBias[3] = {150.0f, 338.0f, -22.0f};
float defMagScale[3] = {1.0592, 0.9651, 0.9806};

char config_file[14] = "calibfile.txt";

void init_parameters (struct config_file_parameters *_parms)
{
  for(uint8_t i = 0; i < 3; i++) {
      _parms->magScale[i] = defMagScale[i];
      _parms->magBias[i] = defMagBias[i];
  }
  strncpy(_parms->httpport,"8080", 6);
}

char *trim (char * s)
{
  /* Initialize start, end pointers */
  char *s1 = s, *s2 = &s[strlen (s) - 1];

  /* Trim and delimit right side */
  while ( (isspace (*s2)) && (s2 >= s1) ) s2--;
  *(s2+1) = '\0';

  /* Trim left side */
  while ( (isspace (*s1)) && (s1 < s2) ) s1++;

  /* Copy finished string */
  strcpy (s, s1);
  return s;
}

uint8_t save_config_params(const char *_file, struct config_file_parameters *_parms) {
    FILE *fp = fopen (_file, "w");
    printf("create / update config file: %s\r\n", _file);

    if (fp == NULL) {
        printf("FAILED to create / update config file!\r\n");
        return 1;
    }

    fprintf (fp, "httpport=%s\n",(char *)(_parms->httpport));

    fprintf (fp, "magScale0=%d\n",(int)(_parms->magScale[0]*PRECNUM));
    fprintf (fp, "magScale1=%d\n",(int)(_parms->magScale[1]*PRECNUM));
    fprintf (fp, "magScale2=%d\n",(int)(_parms->magScale[2]*PRECNUM));

    fprintf (fp, "magBias0=%d\n",(int)(_parms->magBias[0]*PRECNUM));
    fprintf (fp, "magBias1=%d\n",(int)(_parms->magBias[1]*PRECNUM));
    fprintf (fp, "magBias2=%d\n",(int)(_parms->magBias[2]*PRECNUM));

    /* Close file */
    fclose (fp);
    return 0;
}

uint8_t parse_config_file(const char *_file, struct config_file_parameters *_parms) {
    char *s, buff[256];
    
    FILE *fp = fopen (_file, "r");
    printf("opening config file: %s\r\n", _file);

    if (fp == NULL) {
        printf("FAILED to open config file!\r\n");
        return 1;
    }

    /* Read next line */
    while ((s = fgets (buff, sizeof buff, fp)) != NULL) {
        /* Skip blank lines and comments */
        if (buff[0] == '\n' || buff[0] == '#')
            continue;

        /* Parse name/value pair from line */
        char name[MAXSTRLEN], value[MAXSTRLEN];
        s = strtok (buff, " =");
        if (s==NULL) continue;
        else strncpy (name, s, MAXSTRLEN);

        trim(name);

        s = strtok (NULL, " =");
        if (s==NULL) continue;
        else strncpy (value, s, MAXSTRLEN);

        trim (value);

        /* Copy into correct entry in parameters struct */
        if (strcmp(name, "magScale0")==0) _parms->magScale[0] = (float)atoi(value) / (float)PRECNUM;
        else if (strcmp(name, "magScale1")==0) _parms->magScale[1] = (float)atoi(value) / (float)PRECNUM;
        else if (strcmp(name, "magScale2")==0) _parms->magScale[2] = (float)atoi(value) / (float)PRECNUM;
        else if (strcmp(name, "magBias0")==0) _parms->magBias[0] = (float)atoi(value) / (float)PRECNUM;
        else if (strcmp(name, "magBias1")==0) _parms->magBias[1] = (float)atoi(value) / (float)PRECNUM;
        else if (strcmp(name, "magBias2")==0) _parms->magBias[2] = (float)atoi(value) / (float)PRECNUM;
        else if (strcmp(name, "httpport")==0) strncpy (_parms->httpport, value, 6);
        else
            printf ("WARNING: %s/%s: Unknown name/value pair!\n", name, value);
    }

    /* Close file */
    fclose (fp);
    return 0;
}

void save_config_file() {
    printf("Saving config_file: %s\r\n", config_file);
    if(save_config_params(config_file, &params)!=0) printf("Failed to save config!\r\n");
    printf("Done.\r\n");
}

void read_config_file(int argc, char *argv[]) {
    if (argc > 1) strncpy(config_file, argv[1], strlen(argv[1]));

    printf ("Initializing parameters to default values...\n");
    init_parameters(&params);

    printf ("Reading config file...\n");
    if(parse_config_file(config_file, &params)!=0) save_config_params(config_file, &params);

    printf ("config:\r\n");
    printf ("httpport: %s\r\n", (char *)params.httpport);
    printf ("magBias: %2.1f, %2.1f, %2.1f\r\n", params.magBias[0], params.magBias[1], params.magBias[2]);
    printf ("magScale: %2.1f, %2.1f, %2.1f\r\n", params.magScale[0], params.magScale[1], params.magScale[2]);
}

void set_config_magScale(float *_scale) {
    for(uint8_t i=0; i < 3; i++) params.magScale[i] = _scale[i];
}

void get_config_magScale(float *_scale) {
    for(uint8_t i=0; i < 3; i++) _scale[i] = params.magScale[i];
}

void set_config_magBias(float *_bias){
    for(uint8_t i=0; i < 3; i++) params.magBias[i] = _bias[i];
}

void get_config_magBias(float *_bias) {
    for(uint8_t i=0; i < 3; i++) _bias[i] = params.magBias[i];
}

void get_config_httpport(char *_port) {
    memcpy(_port, params.httpport, sizeof(char)*6);
}

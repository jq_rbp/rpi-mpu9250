#include <libwebsockets.h>
#include <string.h>
#include <pthread.h>
#include <stdio.h>
#include "configUtils.h"
#include "webUtils.h"
#include "ocvCam.h"
#include "sensor.h"

struct lws_context *context;
static uint8_t lws_sendFlagIdx = LWS_SEND_DATA;

static bool web_run_flag = true;
static bool web_end_flag = false;
static float quat[4];
static float compass_heading;
static float ocvtime;
static uint8_t ocvidx;
static uint16_t imgb64_width;
static uint16_t imgb64_height;
static char *imgb64_data;
static char *inBuf;
static float magval[3], magbias[3], magscale[3];
static unsigned long magcalnum_max, magcalnum_idx;
static char httpport[6]; //up to 2^16

/* one of these created for each message in the ringbuffer */

struct msg {
	void *payload; /* is malloc'd */
	size_t len;
};

/*
 * One of these is created for each client connecting to us.
 *
 * It is ONLY read or written from the lws service thread context.
 */

struct per_session_data__minimal {
	struct per_session_data__minimal *pss_list;
	struct lws *wsi;
	uint32_t tail;
};

/* one of these is created for each vhost our protocol is used with */

struct per_vhost_data__minimal {
	struct lws_context *context;
	struct lws_vhost *vhost;
	const struct lws_protocols *protocol;

	struct per_session_data__minimal *pss_list; /* linked-list of live pss*/
	pthread_t pthread_spam[2];

	pthread_mutex_t lock_ring; /* serialize access to the ring buffer */
	struct lws_ring *ring; /* {lock_ring} ringbuffer holding unsent content */

	const char *config;
	char finished;
};

/*
 * This runs under both lws service and "spam threads" contexts.
 * Access is serialized by vhd->lock_ring.
 */

static void
__minimal_destroy_message(void *_msg)
{
	struct msg *msg = (struct msg *)_msg;

	free(msg->payload);
	msg->payload = NULL;
	msg->len = 0;
}

/*
 * This runs under the "spam thread" thread context only.
 *
 * We spawn two threads that generate messages with this.
 *
 */

static void *
thread_spam(void *d)
{
	struct per_vhost_data__minimal *vhd =
			(struct per_vhost_data__minimal *)d;
	struct msg amsg;
	int len = 128, index = 1, n;

	do {
		/* don't generate output if nobody connected */
		if (!vhd->pss_list)
			goto wait;

		pthread_mutex_lock(&vhd->lock_ring); /* --------- ring lock { */

		/* only create if space in ringbuffer */
		n = (int)lws_ring_get_count_free_elements(vhd->ring);
		if (!n) {
			lwsl_user("dropping!\n");
			goto wait_unlock;
		}

		amsg.payload = malloc(LWS_PRE + len);
		if (!amsg.payload) {
			lwsl_user("OOM: dropping\n");
			goto wait_unlock;
		}

		switch(lws_sendFlagIdx) {
			case LWS_SEND_DATA:
				n = lws_snprintf((char *)amsg.payload + LWS_PRE, len,
							//  "{\"vhd\":\"%s\",\"tid\":\"%p\",\"msg\":%d,\"qw\":%.2f,\"qx\":%.2f,\"qy\":%.2f,\"qz\":%.2f,\"ch\":%.2f,\"ftime\":%.2f}", vhd->config,
							//  (void *)pthread_self(), index++, quat[0], quat[1], quat[2], quat[3],compass_heading,frametime);
							"{\"qw\":%.2f,\"qx\":%.2f,\"qy\":%.2f,\"qz\":%.2f,\"ch\":%.2f,\"cvt\":%.2f,\"cvi\":%d,\"mvx\":%.2f,\"mvy\":%.2f,\"mvz\":\"%.2f\"}", 
							quat[0], quat[1], quat[2], quat[3],compass_heading,ocvtime,(int)ocvidx, magval[0], magval[1], magval[2]);
				break;
			case LWS_SEND_IMGB64:
				n = lws_snprintf((char *)amsg.payload + LWS_PRE, len,
							"{\"imgw\":%d,\"imgh\":%d,\"imgd\":\"%s\"}", (int)imgb64_width, (int)imgb64_height, imgb64_data);
				break;
			case LWS_SEND_MAGCAL:
				n = lws_snprintf((char *)amsg.payload + LWS_PRE, len,
							"{\"mbx\":%.2f,\"mby\":%.2f,\"mbz\":\"%.2f\",\"msx\":%.2f,\"msy\":%.2f,\"msz\":\"%.2f\"}", magbias[0], magbias[1], magbias[2], magscale[0], magscale[1], magscale[2]);
				break;
			case LWS_SEND_MAGVAL:
				n = lws_snprintf((char *)amsg.payload + LWS_PRE, len,
							"{\"mcx\":%lu,\"mci\":%lu,\"mvx\":%.2f,\"mvy\":%.2f,\"mvz\":\"%.2f\"}", magcalnum_max, magcalnum_idx, magval[0], magval[1], magval[2]);
				break;
			default:
				n = lws_snprintf((char *)amsg.payload + LWS_PRE, len,
							"idle: %d", index++);
				break;
		}
		lws_sendFlagIdx = LWS_SEND_NODATA;

		amsg.len = n;
		n = lws_ring_insert(vhd->ring, &amsg, 1);
		if (n != 1) {
			__minimal_destroy_message(&amsg);
			lwsl_user("dropping!\n");
		} else
			/*
			 * This will cause a LWS_CALLBACK_EVENT_WAIT_CANCELLED
			 * in the lws service thread context.
			 */
			lws_cancel_service(vhd->context);

wait_unlock:
		pthread_mutex_unlock(&vhd->lock_ring); /* } ring lock ------- */

wait:
		usleep(100000);

	} while (!vhd->finished);

	lwsl_notice("thread_spam %p exiting\n", (void *)pthread_self());

	pthread_exit(NULL);

	return NULL;
}

/* this runs under the lws service thread context only */

static int
callback_minimal(struct lws *wsi, enum lws_callback_reasons reason,
			void *user, void *in, size_t len)
{
	struct per_session_data__minimal *pss =
			(struct per_session_data__minimal *)user;
	struct per_vhost_data__minimal *vhd =
			(struct per_vhost_data__minimal *)
			lws_protocol_vh_priv_get(lws_get_vhost(wsi),
					lws_get_protocol(wsi));
	const struct lws_protocol_vhost_options *pvo;
	const struct msg *pmsg;
	void *retval;
	int n, m, r = 0;

	switch (reason) {
	case LWS_CALLBACK_PROTOCOL_INIT:
		/* create our per-vhost struct */
		vhd = (struct per_vhost_data__minimal *)lws_protocol_vh_priv_zalloc(lws_get_vhost(wsi),
				lws_get_protocol(wsi),
				sizeof(struct per_vhost_data__minimal));
		if (!vhd)
			return 1;

		pthread_mutex_init(&vhd->lock_ring, NULL);

		/* recover the pointer to the globals struct */
		pvo = lws_pvo_search(
			(const struct lws_protocol_vhost_options *)in,
			"config");
		if (!pvo || !pvo->value) {
			lwsl_err("%s: Can't find \"config\" pvo\n", __func__);
			return 1;
		}
		vhd->config = pvo->value;

		vhd->context = lws_get_context(wsi);
		vhd->protocol = lws_get_protocol(wsi);
		vhd->vhost = lws_get_vhost(wsi);

		vhd->ring = lws_ring_create(sizeof(struct msg), 8,
					    __minimal_destroy_message);
		if (!vhd->ring) {
			lwsl_err("%s: failed to create ring\n", __func__);
			return 1;
		}

		/* start the content-creating threads */

		for (n = 0; n < (int)LWS_ARRAY_SIZE(vhd->pthread_spam); n++)
			if (pthread_create(&vhd->pthread_spam[n], NULL,
					   thread_spam, vhd)) {
				lwsl_err("thread creation failed\n");
				r = 1;
				goto init_fail;
			}
		break;

	case LWS_CALLBACK_PROTOCOL_DESTROY:
init_fail:
		vhd->finished = 1;
		for (n = 0; n < (int)LWS_ARRAY_SIZE(vhd->pthread_spam); n++)
			if (vhd->pthread_spam[n])
				pthread_join(vhd->pthread_spam[n], &retval);

		if (vhd->ring)
			lws_ring_destroy(vhd->ring);

		pthread_mutex_destroy(&vhd->lock_ring);
		break;

	case LWS_CALLBACK_ESTABLISHED:
		/* add ourselves to the list of live pss held in the vhd */
		lws_ll_fwd_insert(pss, pss_list, vhd->pss_list);
		pss->tail = lws_ring_get_oldest_tail(vhd->ring);
		pss->wsi = wsi;
		break;

	case LWS_CALLBACK_CLOSED:
		/* remove our closing pss from the list of live pss */
		lws_ll_fwd_remove(struct per_session_data__minimal, pss_list,
				  pss, vhd->pss_list);
		break;

	case LWS_CALLBACK_SERVER_WRITEABLE:
		pthread_mutex_lock(&vhd->lock_ring); /* --------- ring lock { */

		pmsg = (const struct msg *)lws_ring_get_element(vhd->ring, &pss->tail);
		if (!pmsg) {
			pthread_mutex_unlock(&vhd->lock_ring); /* } ring lock ------- */
			break;
		}

		/* notice we allowed for LWS_PRE in the payload already */
		m = lws_write(wsi, ((unsigned char *)pmsg->payload) + LWS_PRE,
			      pmsg->len, LWS_WRITE_TEXT);
		if (m < (int)pmsg->len) {
			pthread_mutex_unlock(&vhd->lock_ring); /* } ring lock ------- */
			lwsl_err("ERROR %d writing to ws socket\n", m);
			return -1;
		}

		lws_ring_consume_and_update_oldest_tail(
			vhd->ring,	/* lws_ring object */
			struct per_session_data__minimal, /* type of objects with tails */
			&pss->tail,	/* tail of guy doing the consuming */
			1,		/* number of payload objects being consumed */
			vhd->pss_list,	/* head of list of objects with tails */
			tail,		/* member name of tail in objects with tails */
			pss_list	/* member name of next object in objects with tails */
		);

		/* more to do? */
		if (lws_ring_get_element(vhd->ring, &pss->tail))
			/* come back as soon as we can write more */
			lws_callback_on_writable(pss->wsi);

		pthread_mutex_unlock(&vhd->lock_ring); /* } ring lock ------- */
		break;

	case LWS_CALLBACK_RECEIVE:
		inBuf = (char *) in;
		switch(inBuf[0]) {
			case 'i': ocvCam_toggleImageFlag(); break;
			case 'c': ms_calibMag(); break;
			case 'r': lws_sendFlagIdx = LWS_SEND_MAGCAL; break;
		}
		break;

	case LWS_CALLBACK_EVENT_WAIT_CANCELLED:
		if (!vhd)
			break;
		/*
		 * When the "spam" threads add a message to the ringbuffer,
		 * they create this event in the lws service thread context
		 * using lws_cancel_service().
		 *
		 * We respond by scheduling a writable callback for all
		 * connected clients.
		 */
		lws_start_foreach_llp(struct per_session_data__minimal **,
				      ppss, vhd->pss_list) {
			lws_callback_on_writable((*ppss)->wsi);
		} lws_end_foreach_llp(ppss, pss_list);
		break;

	default:
		break;
	}

	return r;
}

//jeki edited from: 128 to 65536
#define LWS_PLUGIN_PROTOCOL_MINIMAL \
	{ \
		"lws-minimal", \
		callback_minimal, \
		sizeof(struct per_session_data__minimal), \
		128, \
		0, NULL, 0 \
	}

static struct lws_protocols protocols[] = {
	{ "http", lws_callback_http_dummy, 0, 0 },
	LWS_PLUGIN_PROTOCOL_MINIMAL,
	{ NULL, NULL, 0, 0 } /* terminator */
};

static const struct lws_http_mount mount = {
	/* .mount_next */		NULL,		/* linked-list "next" */
	/* .mountpoint */		"/",		/* mountpoint URL */
	/* .origin */			"./mount-origin", /* serve from dir */
	/* .def */			"index.html",	/* default filename */
	/* .protocol */			NULL,
	/* .cgienv */			NULL,
	/* .extra_mimetypes */		NULL,
	/* .interpret */		NULL,
	/* .cgi_timeout */		0,
	/* .cache_max_age */		0,
	/* .auth_mask */		0,
	/* .cache_reusable */		0,
	/* .cache_revalidate */		0,
	/* .cache_intermediaries */	0,
	/* .origin_protocol */		LWSMPRO_FILE,	/* files in a dir */
	/* .mountpoint_len */		1,		/* char count */
	/* .basic_auth_login_file */	NULL,
};

/*
 * This demonstrates how to pass a pointer into a specific protocol handler
 * running on a specific vhost.  In this case, it's our default vhost and
 * we pass the pvo named "config" with the value a const char * "myconfig".
 *
 * This is the preferred way to pass configuration into a specific vhost +
 * protocol instance.
 */

static const struct lws_protocol_vhost_options pvo_ops = {
	NULL,
	NULL,
	"config",		/* pvo name */
	(const char *)"myconfig"	/* pvo value */
};

static const struct lws_protocol_vhost_options pvo = {
	NULL,		/* "next" pvo linked-list */
	&pvo_ops,	/* "child" pvo linked-list */
	"lws-minimal",	/* protocol name we belong to on this vhost */
	""		/* ignored */
};

void set_lwsMagVal (float *_data) {
	memcpy(magval, _data, sizeof(float)*3);
}

void set_lwsMagCalNum(int16_t *_data, unsigned long _max, unsigned long _idx) {
	for(uint8_t i=0;i<3;i++)magval[i] = (float)_data[i];
	magcalnum_max = _max;
	magcalnum_idx = _idx;
	if(lws_sendFlagIdx == LWS_SEND_NODATA)lws_sendFlagIdx = LWS_SEND_MAGVAL;
}

void set_lwsMagCal (float *_bias, float *_scale) {
	memcpy(magbias, _bias, sizeof(float)*3);
	memcpy(magscale, _scale, sizeof(float)*3);
	if(lws_sendFlagIdx == LWS_SEND_NODATA)lws_sendFlagIdx = LWS_SEND_MAGCAL;
}

void set_lwsImgB64 (uint16_t _w, uint16_t _h, const char *_data) {
	imgb64_data = (char*)_data;
	imgb64_width = _w;
	imgb64_height = _h;
	if(lws_sendFlagIdx == LWS_SEND_NODATA)lws_sendFlagIdx = LWS_SEND_IMGB64;
}

void set_web_ocvTimeIdx(float _time, uint8_t _idx) {
	ocvtime = _time;
	ocvidx = _idx;
}

void set_web_quat_heading(float *_q, float _heading) {
	memcpy(quat, _q, sizeof(float)*4);
	compass_heading = _heading;
	if(lws_sendFlagIdx == LWS_SEND_NODATA)lws_sendFlagIdx = LWS_SEND_DATA;
}

void set_web_run_flag (bool _flag) {
    web_run_flag = _flag;
}

void set_web_end_flag (bool _flag) {
    web_end_flag = _flag;
}

bool get_web_run_flag (void) {
    return web_run_flag;
}

bool get_web_end_flag (void) {
    return web_end_flag;
}

void *http_func(void *arg) {
	web_end_flag = false;
    while (web_run_flag) {
        if (lws_service(context, 0)) web_run_flag = false;
    }
    
	lws_context_destroy(context);
	printf("lws service destroyed!\r\n");
	web_end_flag = true;
	return NULL;
}

int init_web(void) {
	struct lws_context_creation_info info;
	int logs = LLL_USER | LLL_ERR | LLL_WARN | LLL_NOTICE
			/* for LLL_ verbosity above NOTICE to be built into lws,
			 * lws must have been configured and built with
			 * -DCMAKE_BUILD_TYPE=DEBUG instead of =RELEASE */
			/* | LLL_INFO */ /* | LLL_PARSER */ /* | LLL_HEADER */
			/* | LLL_EXT */ /* | LLL_CLIENT */ /* | LLL_LATENCY */
			/* | LLL_DEBUG */;

	lws_set_log_level(logs, NULL);
	// lwsl_user("LWS minimal ws server + threads | visit http://localhost:7681\n");

	memset(&info, 0, sizeof info); /* otherwise uninitialized garbage */
	get_config_httpport(httpport);
	info.port = atoi(httpport);
	info.mounts = &mount;
	info.protocols = protocols;
	info.pvo = &pvo; /* per-vhost options */
	info.options =
		LWS_SERVER_OPTION_HTTP_HEADERS_SECURITY_BEST_PRACTICES_ENFORCE;

	context = lws_create_context(&info);
	if (!context) {
		lwsl_err("lws init failed\n");
		return 1;
	}
    
    printf("starting server...\n");
    return 0;
}
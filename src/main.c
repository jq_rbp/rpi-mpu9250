#include "threadUtils.h"
#include "timeUtils.h"
#include "configUtils.h"

int main(int argc, char** argv) {
	read_config_file(argc, argv);
	init_time();
	init_threads();
	while(1) {}

	return 0;
}

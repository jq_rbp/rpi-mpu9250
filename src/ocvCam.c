// #include <opencv/cv.h>
// #include <opencv/highgui.h>
// #include <iostream>

#include <opencv2/opencv.hpp>
#include <iostream>
#include <vector>
#include <stdio.h>
#include <stdint.h>
#include "timeUtils.h"
#include "webUtils.h"

using namespace cv;
using namespace std;

#define MAXFILENUM 10

enum Filetype {
  OCVFILE_EDGE = 0,
  OCVFILE_GRAY,
  OCVFILE_COLOR,
  OCVFILE_MAX
};

float fDeltaTimeMs;
uint32_t u32NowTime;
uint32_t u32LastUpdate;

VideoCapture cap = VideoCapture(); // open the default camera
BackgroundSubtractorMOG2 bg; // background subtractor
vector < vector < cv::Point > >contours;

Mat grayMat;
Mat scaledMat;
Mat frameMat;
Mat resMat;

static uint8_t saveResImageIdx = 0;

// filename: "mount-origin/assets/images/ocv/ocv_image_255.jpg"
char filenameBuf[50];
uint8_t curSavedFileIdx;
uint8_t curDelFileIdx;
uint8_t camIdx = 0;
int ocvCam_init(void)
{
    while(!cap.isOpened()) {
        cap.open(camIdx);
        camIdx++;
        if(camIdx > 10) break;
    }
    if(!cap.isOpened())  // check if we succeeded
    {
        cout << "Error opening camera!";
        return -1;
    }
    curSavedFileIdx = 0;
    curDelFileIdx = 0;
    bg.set ("nmixtures", 3);
    // the camera will be deinitialized automatically in VideoCapture destructor
    return 0;
}

void ocvCam_toggleImageFlag(void) {
    saveResImageIdx++;
    if(saveResImageIdx >= (uint8_t)OCVFILE_MAX) saveResImageIdx = 0;
}

void deleteFile(uint8_t _idx) {
    sprintf(filenameBuf, "mount-origin/assets/images/ocv/ocv_image_%d.jpg", _idx);
    if (remove(filenameBuf) != 0)
      printf("Unable to delete the file idx: %d\r\n", _idx);
}

void saveMatToFile(void) {
    sprintf(filenameBuf, "mount-origin/assets/images/ocv/ocv_image_%d.jpg", curSavedFileIdx);
    switch(saveResImageIdx) {
        case OCVFILE_EDGE:
            imwrite(filenameBuf, resMat);
            // if(contours.size() > 0) {
            //     set_lwsImgB64 (160, 120, ocvB64Convert.mat2str(resMat).c_str());
            // }
            break;
        case OCVFILE_COLOR:
            findContours (resMat, contours, CV_RETR_EXTERNAL,
                CV_CHAIN_APPROX_SIMPLE);
            drawContours (scaledMat, contours, -1, cv::Scalar (0, 0, 255), 1);
            // set_lwsImgB64 (160, 120, ocvB64Convert.mat2str(scaledMat).c_str());
            imwrite(filenameBuf, scaledMat);
            break;
        case OCVFILE_GRAY:
            findContours (resMat, contours, CV_RETR_EXTERNAL,
                CV_CHAIN_APPROX_SIMPLE);
            drawContours (grayMat, contours, -1, cv::Scalar (0, 0, 255), 1);
            // set_lwsImgB64 (160, 120, ocvB64Convert.mat2str(grayMat).c_str());
            imwrite(filenameBuf, grayMat);
            break;
        default:
            break;
    }
    
    set_web_ocvTimeIdx(fDeltaTimeMs, curSavedFileIdx);

    if(!((curSavedFileIdx < MAXFILENUM) && (curDelFileIdx == 0))) {
        deleteFile(curDelFileIdx);
        if(curDelFileIdx < 0xFF) curDelFileIdx++;
        else curDelFileIdx = 0;
    }
    if(curSavedFileIdx < 0xFF) curSavedFileIdx++;
    else curSavedFileIdx = 0;
}

void ocvCam_imgProcess() {
    u32NowTime = micros();
    fDeltaTimeMs = ((float)(u32NowTime - u32LastUpdate) / 1000.0f);
    u32LastUpdate = u32NowTime;
    cap >> frameMat; // get a new frame from camera
    resize(frameMat, scaledMat, Size(160, 120), 0, 0, cv::INTER_AREA);
    cvtColor(scaledMat, grayMat, CV_BGR2GRAY);
    // GaussianBlur(grayMat, resMat, Size(7,7), 1.5, 1.5);
    // Canny(resMat, resMat, 0, 30, 3);
    bg.operator()(grayMat, resMat);
    erode (resMat, resMat, Mat());
    dilate (resMat, resMat, Mat());
}

void ocvCam_loop(void){
    ocvCam_imgProcess();
    saveMatToFile();
}

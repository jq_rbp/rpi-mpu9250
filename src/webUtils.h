#ifndef _WEB_UTILS_H_
#define _WEB_UTILS_H_

enum LwsSendFlag{
    LWS_SEND_NODATA = 0,
    LWS_SEND_DATA,
    LWS_SEND_IMGB64,
    LWS_SEND_MAGVAL,
    LWS_SEND_MAGCAL
};

void set_lwsMagVal (float *_data);
void set_lwsMagCalNum(int16_t *_data, unsigned long _max, unsigned long _idx);
void set_lwsMagCal (float *_bias, float *_scale);
void set_lwsImgB64 (uint16_t _w, uint16_t _h, const char *_data);
void set_web_ocvTimeIdx(float _time, uint8_t _idx);
void set_web_quat_heading(float *_q, float _heading);
void set_web_run_flag (bool _flag);
void set_web_end_flag (bool _flag);
bool get_web_run_flag (void);
bool get_web_end_flag (void);
void *http_func(void *arg);
int init_web(void);

#endif //_WEB_UTILS_H_
#ifndef __OCVCAM_H__
#define __OCVCAM_H__

void ocvCam_toggleImageFlag(void);
int ocvCam_init(void);
void ocvCam_loop(void);
#endif
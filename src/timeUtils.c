#include <time.h>
#include <errno.h>
#include <stdint.h>

clock_t start_t;

void init_time(void) {
    start_t = clock();
}

int delay(unsigned long msec)
{
    struct timespec ts;
    int res;

    if (msec < 0)
    {
        errno = EINVAL;
        return -1;
    }

    ts.tv_sec = msec / 1000;
    ts.tv_nsec = (msec % 1000) * 1000000;

    do {
        res = nanosleep(&ts, &ts);
    } while (res && errno == EINTR);

    return res;
}

int delay_ms(unsigned long ms) {
    return delay(ms);
}

unsigned long micros(void) {
    clock_t curr_t;
    curr_t = clock();
    return (curr_t - start_t);
}

unsigned long millis(void) {
    return micros() / 1000;
}

void get_ms(unsigned long *_ms) {
    *_ms = millis();
}

unsigned long delta_ms(unsigned long _ms) {
    return millis() - _ms;
}
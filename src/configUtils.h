#ifndef __CONFIG_UTILS_H__
#define __CONFIG_UTILS_H__

struct config_file_parameters {
    float magScale[3];
    float magBias[3];
    char httpport[6];
};

void save_config_file();
void read_config_file(int argc, char *argv[]);
void set_config_magScale(float *_scale);
void get_config_magScale(float *_scale);
void set_config_magBias(float *_bias);
void get_config_magBias(float *_bias);
void get_config_httpport(char *_port);

#endif //__CONFIG_UTILS_H__
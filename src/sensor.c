#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>

#include "timeUtils.h"
#include "webUtils.h"
#include "eMPL/inv_mpu.h"
#include "eMPL/inv_mpu_dmp_motion_driver.h"
#include "helper_3dmath.h"
#include "sensor.h"
#include "configUtils.h"

#define YAW 0
#define PITCH 2
#define ROLL 1
#define DIM 3

#define wrap_180(x) (x < -180 ? x+360 : (x > 180 ? x - 360: x))
#define RADTODEG(x) ((x) * 57.295779513082320876798154814105f)
#define DEGTORAD(x) ((x) * 0.01745329251994329576923690768489f)
#define MAX_CAL_SAMPLING 100

// MPU control/status vars
uint8_t devStatus;      // return status after each device operation
//(0 = success, !0 = error)
uint8_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[64]; // FIFO storage buffer

unsigned long ulTimeStamp = 0;

int16_t a[3];              // [x, y, z]            accel vector
int16_t g[3];              // [x, y, z]            gyro vector
long _q[4];
long t;
int16_t c[3];

int r;
int initialized = 0;
int dmpReady = 0;
int16_t sensors;

float ypr[3];
float temp;
float gyro[3];
float accel[3];
float compass[3];
float fHeading[2];
float fQ[4];

// sensor sensitivity
float _mSense = 1.0f;//6.667f; // Constant - 4915 / 32760
uint16_t _aSense = 0;//16384;   // Updated after accel FSR is set
float _gSense = 0.f; //16.384f;   // Updated after gyro FSR is set

float accelBias[3] = {0};
float gyroBias[3] = {0};

bool magCalibFlag = false;
uint32_t magCalibTime = 0;
uint32_t magCalibDT = 0;
uint16_t magCalibIdx = 0;
uint16_t magRate = 8;
float magBias[3] = {150.0f, 338.0f, -22.0f};
float magScale[3] = {1.0592, 0.9651, 0.9806};
int16_t mag_max[3] = {-32767, -32767, -32767}, mag_min[3] = {32767, 32767, 32767}, mag_temp[3] = {0, 0, 0};

bool showDataFlag = true;

uint8_t rate = 40; // ~20ms
// uint8_t rate = 80;

static float lastDMPYaw = 0;
static float lastYaw = 0;
static float yaw_mixing_factor = 50.0f;
static Quaternion rawQ;
static VectorFloat calMag;
static Quaternion fusedQ;

int data_fusion(Quaternion *rawQ, VectorFloat *calMag, Quaternion *fusedQuat);

float GetCompassHeading(float acc_x, float acc_y, float acc_z, float mag_x, float mag_y, float mag_z) {
// float GetCompassHeading(float Y_r, float X_r, float mag_x, float mag_y, float mag_z) {
	double Y_r, X_r, X_h, Y_h, azimuth;
	double cosY_r, sinY_r,cosX_r,sinX_r;

	// pitch is along Y-Axis using XYZ rotation
	Y_r = atan2((double)-acc_x, sqrt((long)acc_z*(long)acc_z + (long)acc_y*(long)acc_y));
	// roll is along X-Axis
	if(acc_z < 0) X_r = atan2((double)acc_y, -sqrt((long)acc_z*(long)acc_z  + (long)acc_x*(long)acc_x));
	else X_r = atan2((double)acc_y, sqrt((long)acc_z*(long)acc_z  + (long)acc_x*(long)acc_x));
	// // pitch = (double)_pitch; roll = (double)_roll;
	
	/* Calculate Azimuth:
		* Magnetic horizontal components, after compensating for Roll(r) and Pitch(p) are:
		* X_h = X*cos(p) + Y*sin(r)*sin(p) + Z*cos(r)*sin(p)
		* Y_h = Y*cos(r) - Z*sin(r)
		* Azimuth = arcTan(Y_h/X_h)
		*/
	cosY_r = cos(Y_r);
	sinY_r = sin(Y_r);
	cosX_r = cos(X_r);
	sinX_r = sin(X_r);

	X_h = (double)mag_x*cosY_r + (double)mag_y*sinX_r*sinY_r + (double)mag_z*cosX_r*sinY_r;
	Y_h = (double)mag_y*cosX_r - (double)mag_z*sinX_r;
	azimuth = atan2(Y_h, X_h);

	if(azimuth < 0) {	/* Convert Azimuth in the range (0, 2pi) */
		azimuth = 2*M_PI + azimuth;
	}
	return (float)azimuth;
}

int ms_open() {
	dmpReady=1;
	initialized = 0;

	// initialize device
	printf("Initializing MPU...\n");
	if (mpu_init(NULL) != 0) {
		printf("MPU init failed!\n");
		return -1;
	}
	printf("Setting MPU sensors...\n");

	#if defined MPU9250 || defined MPU9150
	printf("Setting MPU sensors (INV_XYZ_GYRO|INV_XYZ_ACCEL|INV_XYZ_COMPASS)...\n");
	if (mpu_set_sensors(INV_XYZ_GYRO|INV_XYZ_ACCEL|INV_XYZ_COMPASS)!=0) {
		printf("Failed to set sensors!\n");
		return -1;
	}
	#else
	printf("Setting MPU sensors (INV_XYZ_GYRO|INV_XYZ_ACCEL)...\n");
	if (mpu_set_sensors(INV_XYZ_GYRO|INV_XYZ_ACCEL)!=0) {
		printf("Failed to set sensors!\n");
		return -1;
	}
	#endif
	printf("Setting GYRO sensitivity...\n");
	if (mpu_set_gyro_fsr(2000)!=0) {
		printf("Failed to set gyro sensitivity!\n");
		return -1;
	}
	printf("Setting ACCEL sensitivity...\n");
	if (mpu_set_accel_fsr(2)!=0) {
		printf("Failed to set accel sensitivity!\n");
		return -1;
	}
	// verify connection
	printf("Powering up MPU...\n");
	mpu_get_power_state(&devStatus);
	printf(devStatus ? "MPU connection successful\n" : "MPU connection failed %u\n",devStatus);

	//fifo config
	printf("Setting MPU fifo...\n");
	if (mpu_configure_fifo(INV_XYZ_GYRO|INV_XYZ_ACCEL)!=0) {
		printf("Failed to initialize MPU fifo!\n");
		return -1;
	}

	// load and configure the DMP
	printf("Loading DMP firmware...\n");
	if (dmp_load_motion_driver_firmware()!=0) {
		printf("Failed to enable DMP!\n");
		return -1;
	}

	printf("Activating DMP...\n");
	if (mpu_set_dmp_state(1)!=0) {
		printf("Failed to enable DMP!\n");
		return -1;
	}

	//dmp_set_orientation()
	// dmp_set_orientation(inv_orientation_matrix_to_scalar(gyro_orientation));
	//if (dmp_enable_feature(DMP_FEATURE_LP_QUAT|DMP_FEATURE_SEND_RAW_GYRO)!=0) {
	printf("Configuring DMP...\n");
	if (dmp_enable_feature(DMP_FEATURE_6X_LP_QUAT|DMP_FEATURE_SEND_RAW_ACCEL|DMP_FEATURE_SEND_CAL_GYRO|DMP_FEATURE_GYRO_CAL)!=0) {
	// if (dmp_enable_feature(DMP_FEATURE_6X_LP_QUAT|DMP_FEATURE_SEND_RAW_ACCEL|DMP_FEATURE_SEND_RAW_GYRO)!=0) {
		printf("Failed to enable DMP features!\n");
		return -1;
	}


	printf("Setting DMP fifo rate...\n");
	if (dmp_set_fifo_rate(rate)!=0) {
		printf("Failed to set dmp fifo rate!\n");
		return -1;
	}
	printf("Resetting fifo queue...\n");
	if (mpu_reset_fifo()!=0) {
		printf("Failed to reset fifo!\n");
		return -1;
	}

	printf("Checking... ");
	do {
		delay_ms((unsigned long)(1000/rate));  //dmp will habve 4 (5-1) packets based on the fifo_rate
		r=dmp_read_fifo(g,a,_q,&ulTimeStamp,&sensors,&fifoCount);
	} while (r!=0 || fifoCount<5); //packtets!!!
	printf("Done.\n");

	mpu_get_accel_sens(&_aSense);
	mpu_get_gyro_sens(&_gSense);

	printf("asens: %d, gsens: %2.1f\r\n", _aSense, _gSense);
	initialized = 1;

	get_config_magScale(magScale);
	get_config_magBias(magBias);

	set_lwsMagCal(magBias, magScale);

	return 0;
}

int dmp_update_nonblocking() {
	if (!dmpReady) {
		printf("Error: DMP not ready!!\n");
		return -1;
	}

	if (dmp_read_fifo(g,a,_q,&ulTimeStamp,&sensors,&fifoCount)!=0) return 1; //gyro and accel can be null because of being disabled in the efeatures

	mpu_get_compass_reg(c,&ulTimeStamp);
	mpu_get_temperature(&t,&ulTimeStamp);
	temp=(float)t/65536L;

	//0=gyroX, 1=gyroY, 2=gyroZ
	//swapped to match Yaw,Pitch,Roll
	//Scaled from deg/s to get tr/s
	for (int i=0;i<DIM;i++){
		gyro[i]   = (float)(g[DIM-i-1])/_gSense - gyroBias[DIM-i-1];
		accel[i]   = (float)(a[DIM-i-1]);//(float)_aSense;
		compass[i] = (float)(c[DIM-i-1]) - magBias[DIM-i-1];
		compass[i] *= magScale[DIM-i-1];
	}

	// fHeading[1] = RADTODEG(GetCompassHeading(
	// 	DEGTORAD(ypr[ROLL]), DEGTORAD(ypr[PITCH]),
	// 	compass[ROLL], compass[PITCH], -compass[YAW]));

	fHeading[1] = RADTODEG(GetCompassHeading(
		-accel[1], accel[2], accel[0],
		compass[2], -compass[1], compass[0]));

	if(fHeading[1] - fHeading[0] > 180.0f) {
		fHeading[0] += 360;
	} else {
		if(fHeading[0] - fHeading[1] > 180.0f) {
			fHeading[0] -= 360;
		}
	}
	// fHeading[1] = RADTODEG(atan2((double)compass[ROLL], (double)compass[PITCH]));
	fHeading[0] = (fHeading[0] * 41.0f + fHeading[1]) / 42.0f;

	rawQ.w = _q[0];rawQ.x = _q[1];rawQ.y = _q[2];rawQ.z = _q[3];
	rawQ.normalize();
	// calMag.x = compass[2];calMag.y = -compass[1];calMag.z = compass[0];

	// data_fusion(&rawQ, &calMag, &fusedQ);

	// fQ[0] = fusedQ.w; fQ[1] = fusedQ.x; fQ[2] = fusedQ.y; fQ[3] = fusedQ.z;
	// for(uint8_t i = 0; i < 4; i++) fQ[i] = (float)_q[i];
	fQ[0] = rawQ.w; fQ[1] = rawQ.x; fQ[2] = rawQ.y; fQ[3] = rawQ.z;
	return 0;
}

void ms_loop() {
	if(magCalibFlag) magcalMPU9250_nonblocking(magBias,magScale);
	else {
		dmp_update_nonblocking();
		set_lwsMagVal(compass);
		set_web_quat_heading(fQ, fHeading[0]);
	}
	if(showDataFlag) {
		printf("yaw = %2.1f\tpitch = %2.1f\troll = %2.1f\ttemperature = %2.1f\tg = %2.1f, %2.1f, %2.1f\ta = %2.1f, %2.1f, %2.1f\tcompass = %2.1f, %2.1f, %2.1f\thead = %2.1f\n",
		ypr[YAW], ypr[PITCH], ypr[ROLL],temp,
		gyro[PITCH], gyro[ROLL], gyro[YAW],
		accel[PITCH], accel[ROLL], accel[YAW],
		compass[PITCH],compass[ROLL],compass[YAW], fHeading[0]);
	}
}

void ms_changePrintMode() {
};

void ms_toggleShowData(){
	showDataFlag = !showDataFlag;
}

void ms_showCalib() {
	showDataFlag = false;
	printf("\r\nmagBias: %2.4f, %2.4f, %2.4f\r\n", (float)magBias[0], (float)magBias[1], (float)magBias[2]);
	printf("magScale: %2.4f, %2.4f, %2.4f\r\n", (float)magScale[0], (float)magScale[1], (float)magScale[2]);
}

int magcalMPU9250_nonblocking(float * dest1, float * dest2) {
	uint16_t sample_count = 0;
	int32_t mag_bias[3] = {0, 0, 0}, mag_scale[3] = {0, 0, 0};

	if(magCalibTime == 0) {
		printf("Mag Calibration: Wave device in a figure eight until done!\r\n");
		if(mpu_get_compass_sample_rate(&magRate)!=0) {
			printf("cannot read compass sample rate\r\n");
			return 1;
		};
		printf("Mag sample rate: %d\r\n", magRate);
		for (uint8_t i = 0; i < 3; i++) {
			mag_max[i] = -32767;
			mag_min[i] = 32767;
		}
		magCalibTime = millis();
		magCalibDT = 4000;
	} else {
		if(delta_ms(magCalibTime) >= magCalibDT) {
			// shoot for ~fifteen seconds of mag data
			if(magRate <= 10) sample_count = 128;  // at 8 Hz ODR, new mag data is available every 125 ms
			else sample_count = 1500;  // at 100 Hz ODR, new mag data is available every 10 ms
			
			if(magCalibIdx < sample_count) {
				if(mpu_get_compass_reg(mag_temp, &ulTimeStamp)!=0) {
					printf("cannot get mag data");
				} // Read the mag data
				for (int jj = 0; jj < 3; jj++) {
					if(mag_temp[jj] > mag_max[jj]) mag_max[jj] = mag_temp[jj];
					if(mag_temp[jj] < mag_min[jj]) mag_min[jj] = mag_temp[jj];
				}
				set_lwsMagCalNum(mag_temp, sample_count, magCalibIdx);
				printf(".");
				if(magRate <= 10) magCalibDT = 135;  // at 8 Hz ODR, new mag data is available every 125 ms
				else magCalibDT = 12;  // at 100 Hz ODR, new mag data is available every 10 ms
				magCalibIdx++;
				magCalibTime = millis();
			} else {
				//    Serial.println("mag x min/max:"); Serial.println(mag_max[0]); Serial.println(mag_min[0]);
				//    Serial.println("mag y min/max:"); Serial.println(mag_max[1]); Serial.println(mag_min[1]);
				//    Serial.println("mag z min/max:"); Serial.println(mag_max[2]); Serial.println(mag_min[2]);

				// Get hard iron correction
				mag_bias[0]  = (mag_max[0] + mag_min[0])/2;  // get average x mag bias in counts
				mag_bias[1]  = (mag_max[1] + mag_min[1])/2;  // get average y mag bias in counts
				mag_bias[2]  = (mag_max[2] + mag_min[2])/2;  // get average z mag bias in counts

				dest1[0] = (float) mag_bias[0];  // save mag biases in G for main program
				dest1[1] = (float) mag_bias[1]; 
				dest1[2] = (float) mag_bias[2];

				// Get soft iron correction estimate
				mag_scale[0]  = (mag_max[0] - mag_min[0])/2;  // get average x axis max chord length in counts
				mag_scale[1]  = (mag_max[1] - mag_min[1])/2;  // get average y axis max chord length in counts
				mag_scale[2]  = (mag_max[2] - mag_min[2])/2;  // get average z axis max chord length in counts

				float avg_rad = mag_scale[0] + mag_scale[1] + mag_scale[2];
				avg_rad /= 3.0;

				dest2[0] = avg_rad/((float)mag_scale[0]);
				dest2[1] = avg_rad/((float)mag_scale[1]);
				dest2[2] = avg_rad/((float)mag_scale[2]);

				set_config_magBias(dest1);
				set_config_magScale(dest2);
				save_config_file();

				set_lwsMagCal(dest1, dest2);

				printf("Mag Calibration done!\r\n");
				printf("bias: %2.4f, %2.4f, %2.4f\tscale: %2.4f, %2.4f, %2.4f\r\n", 
					(float)dest1[0], (float)dest1[1], (float)dest1[2],
					(float)dest2[0], (float)dest2[1], (float)dest2[2]);
				magCalibFlag = false;
				magCalibTime = 0;
				magCalibIdx = 0;
			}
		}
	}
	return 0;
}  

void ms_calibMag() {
	magCalibFlag = true;
};

int ms_close() {
	return 0;
}

void quat2Euler(Quaternion *q, VectorFloat *v)
{
	// fix roll near poles with this tolerance
	float pole = (float)M_PI / 2.0f - 0.05f;

	v->y = asinf(2.0f * (q->w * q->y - q->x * q->z));

	if ((v->y < pole) && (v->y > -pole)) {
		v->x = atan2f(2.0f * (q->y * q->z + q->w * q->x),
					1.0f - 2.0f * (q->x * q->x + q->y * q->y));
	}

	v->z = atan2f(2.0f * (q->x * q->y + q->w * q->z),
					1.0f - 2.0f * (q->y * q->y + q->z * q->z));
}

void euler2Quat(VectorFloat *v, Quaternion *q)
{
	float cosX2 = cosf(v->x / 2.0f);
	float sinX2 = sinf(v->x / 2.0f);
	float cosY2 = cosf(v->y / 2.0f);
	float sinY2 = sinf(v->y / 2.0f);
	float cosZ2 = cosf(v->z / 2.0f);
	float sinZ2 = sinf(v->z / 2.0f);

	q->w = cosX2 * cosY2 * cosZ2 + sinX2 * sinY2 * sinZ2;
	q->x = sinX2 * cosY2 * cosZ2 - cosX2 * sinY2 * sinZ2;
	q->y = cosX2 * sinY2 * cosZ2 + sinX2 * cosY2 * sinZ2;
	q->z = cosX2 * cosY2 * sinZ2 - sinX2 * sinY2 * cosZ2;

	q->normalize();
}

void tilt_compensate(Quaternion *magQ, Quaternion *unfusedQ)
{
	Quaternion unfusedConjugateQ;
	Quaternion tempQ;

	unfusedConjugateQ = unfusedQ->getConjugate();
	tempQ = magQ->getProduct(unfusedConjugateQ);
    *magQ = unfusedQ->getProduct(tempQ);
}

int data_fusion(Quaternion *rawQ, VectorFloat *calMag, Quaternion *fusedQuat)
{
	Quaternion dmpQuat = Quaternion(rawQ->w, rawQ->x, rawQ->y, rawQ->z);
	VectorFloat dmpEuler, fusedEuler;
	Quaternion magQuat;
	Quaternion unfusedQuat;

	float deltaDMPYaw;
	float deltaMagYaw;
	float newMagYaw;
	float newYaw;
	
    dmpQuat.normalize();
	quat2Euler(&dmpQuat, &dmpEuler);

	fusedEuler.x = dmpEuler.x;
	fusedEuler.y = -dmpEuler.y;
	fusedEuler.z = 0;

	euler2Quat(&fusedEuler, &unfusedQuat);

	deltaDMPYaw = dmpEuler.z - lastDMPYaw;
	lastDMPYaw = dmpEuler.z;

	magQuat.w = 0;
	magQuat.x = calMag->x;
  	magQuat.y = calMag->y;
  	magQuat.z = calMag->z;

	tilt_compensate(&magQuat, &unfusedQuat);

	newMagYaw = -atan2f(magQuat.y, magQuat.x);

	if (newMagYaw != newMagYaw) {
		printf("newMagYaw NAN\n");
		return -1;
	}

	if (newMagYaw < 0.0f)
		newMagYaw = TWO_PI + newMagYaw;

	newYaw = lastYaw + deltaDMPYaw;

	if (newYaw > TWO_PI)
		newYaw -= TWO_PI;
	else if (newYaw < 0.0f)
		newYaw += TWO_PI;
	 
	deltaMagYaw = newMagYaw - newYaw;
	
	if (deltaMagYaw >= (float)M_PI)
		deltaMagYaw -= TWO_PI;
	else if (deltaMagYaw < -(float)M_PI)
		deltaMagYaw += TWO_PI;

	if (yaw_mixing_factor > 0)
		newYaw += deltaMagYaw / yaw_mixing_factor;

	if (newYaw > TWO_PI)
		newYaw -= TWO_PI;
	else if (newYaw < 0.0f)
		newYaw += TWO_PI;

	lastYaw = newYaw;

	if (newYaw > (float)M_PI)
		newYaw -= TWO_PI;

	fusedEuler.z = newYaw;

	euler2Quat(&fusedEuler, fusedQuat);

	return 0;
}

#ifndef SENSOR_H
#define SENSOR_H

int ms_open();
void ms_loop();
int magcalMPU9250_nonblocking(float * dest1, float * dest2);
void ms_changePrintMode();
void ms_toggleShowData();
void ms_showCalib();
void set_magCalibFlag(bool _flag);
void ms_calibMag();
int ms_close();

#endif

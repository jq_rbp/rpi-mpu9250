#ifndef _TIME_UTILS_H_
#define _TIME_UTILS_H_

#include <stdint.h>

void init_time(void);
int delay(unsigned long msec);
int delay_ms(unsigned long ms);
unsigned long micros(void);
unsigned long millis(void);
void get_ms(unsigned long *_ms);
unsigned long delta_ms(unsigned long _ms);
#endif //_TIME_UTILS_H_
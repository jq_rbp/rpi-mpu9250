#include <signal.h>
#include <iostream>
#include <unistd.h>

#include "timeUtils.h"
#include "sensor.h"
#include "webUtils.h"
#include "ocvCam.h"

bool sensors_run_flag = true;
bool keypress_run_flag = true;
bool alarm_run_flag = false;
bool ocvCam_run_flag = true;

void *keypress_func(void *arg) {
    int c;
    while(keypress_run_flag) {
        alarm(2); alarm_run_flag = false;
        if(keypress_run_flag) {
            c = getchar();
            switch(c){
                case '\t':
                    ms_changePrintMode();
                    break;
                case 'c':
                    ms_calibMag();
                    break;
                case 'p':
                    ms_toggleShowData();
                    break;
                case 's':
                    ms_showCalib();
                    break;
                default: 
                    break;
            }
        }
    }

    keypress_run_flag = !keypress_run_flag;

    return NULL;
}

void *sensors_func(void *arg) {
    while(sensors_run_flag)ms_loop();
    sensors_run_flag = !sensors_run_flag;
    return NULL;
}

void *ocvCam_func(void *arg) {
    while(ocvCam_run_flag) {
        ocvCam_loop();
    }

    ocvCam_run_flag = !ocvCam_run_flag;

    return NULL;
}

void threads_signal_handle(int s) {
    printf("waiting threads to exit\r\n");
    sensors_run_flag = false;
    while(!sensors_run_flag) delay_ms(10);
    printf("sensor thread finished\r\n");

    keypress_run_flag = false;
    while(!keypress_run_flag & !alarm_run_flag) delay_ms(10);
    printf("keypress thread finished\r\n");

    ocvCam_run_flag = false;
    while(!ocvCam_run_flag) delay_ms(10);
    printf("ocvCam thread finished\r\n");

    set_web_run_flag(false);
    printf("waiting for the web thread to finish\r\n");
    while(!get_web_end_flag()){
        delay_ms(1000);
    }
    // set_web_run_flag(true);
    printf("web thread finished\r\n");

    printf("threads exited\r\n");

    exit(s);
}

void  ALARMhandler(int sig)
{
  signal(SIGALRM, SIG_IGN);          /* ignore this signal       */
  alarm_run_flag = true;
  delay_ms(10);
  signal(SIGALRM, ALARMhandler);     /* reinstall the handler    */
}

void init_threads(void) {
    pthread_t sensor_th;
    pthread_t keypress_th;
    pthread_t http_th;
    pthread_t ocvCam_th;

    signal(SIGINT, threads_signal_handle);
	signal(SIGTERM, threads_signal_handle);
    signal(SIGALRM, ALARMhandler);

    ms_open(); //initialize sensor
    pthread_create(&sensor_th,NULL,sensors_func,(void *)"starting sensor thread ...");

    pthread_create(&keypress_th,NULL,keypress_func,(void *)"starting key press thread ...");

    if(!init_web())
        pthread_create(&http_th,NULL,http_func,(void *)"starting http thread ...");
    
    if(ocvCam_init() == 0)
        pthread_create(&ocvCam_th,NULL,ocvCam_func,(void *)"starting cam thread ...");
}